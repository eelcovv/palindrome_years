import pandas as pd
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description="Check of we palindromen hebben",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--filename', help="Name of the data file", default="palindrome_vs_year_all.dat")
parser.add_argument('-s', '--start', help="Start year of range", default=0, type=int)
parser.add_argument('-n', '--number', help="Aantal eeuwen van start", default=30, type=int)
parser.add_argument('-p', '--plot', help="Show the image", action="store_true")
args = parser.parse_args()

file_name = args.filename
start = args.start
number = args.number
end = start + number

data_df = pd.read_csv(file_name, names=["start", "end", "count"], sep="\s+")

data_df.plot(y=["count"], legend=False)

data_df["eeuw"] = data_df["start"] / 100
data_df.set_index("eeuw", drop=True, inplace=True)

plt.ylabel("# Palindrooms")
plt.title("# Palindrooms per 100 jaar vs eeuw")

plt.xlim((start, end))


file_name = "palindrome_vs_year_{:04d}_{:04d}.jpg".format(start, end)
print(f"write image {file_name}")
plt.savefig(file_name)

if args.plot:
    plt.show()

