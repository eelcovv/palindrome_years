import sys
import logging
import argparse

logger: logging.Logger = None


def parse_the_argument(args):
    parser = argparse.ArgumentParser(description="Check of we palindromen hebben",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level", const=logging.INFO)
    parser.add_argument('--no_silent', help="Do not suppress the latex output",
                        action="store_false", dest="silent")
    parser.add_argument('--silent', help="Suppress the latex output",
                        action="store_true", default=True)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level", const=logging.WARNING)
    parser.add_argument('-m', '--mode', help="Pick the check mode: divide or palindrome",
                        choices={"palindrome", "divide"}, default="palindrome")
    parser.add_argument('-s', '--start', help="Start year of range", default=2000, type=int)
    parser.add_argument('-n', '--number', help="Number of years from start", default=100, type=int)

    # parse the command line
    parsed_arguments = parser.parse_args(args)

    return parsed_arguments, parser


def calculate_n_palindromes(start):
    """ count the number of palindromes per century """
    digits = list(str(start))
    n_digits = len(digits)

    if n_digits < 3:  # jaren 0-99
        # voor jaren 0 - 9: 5 uit tien
        # voor jaren 10 - 99: Trian(9) + 8 = 53 uit 90 (trian is 9 + 8 + 7 +..)
        n_palin = 58
    else:
        d1 = int(digits[0])
        d2 = int(digits[1])
        d3 = int(digits[2])
        const = 1
        if n_digits == 3:  # jaren 100-999
            if d1 == 1:
                const = 0
            n_palin = 50 - d1 * 5 + const
        else:  # jaren 1000 en meer
            n_palin = 100 - 10 * d1 - 9 * d2 + (d1 - 1) * d2
            if n_digits == 4:  # jaren 1000-9999
                if d1 == 1 or d2 == 1:
                    const = 0
            elif n_digits == 5:  # jaren 10000-99999
                if d3 < 5:
                    if d1 < 2:
                        if d1 == 1 or d2 == 1 or d3 == 1:
                            const = 0
                    else:
                        if d2 == 1 or d3 != 0:
                            const = 0
                else:
                    n_palin = 0
                    const = 0
            else:
                logger.warning("Formule alleen geldig voor minder dan 6 digits ")
                const = 1

            n_palin += const

    return n_palin


def is_palindrome(numint):
    """ Check if the number *numint* is a palindrome """
    numstr = str(numint)
    invstr = numstr[-1::-1]
    return invstr == numstr


def digit_summation(numstr):
    """ get sum of the digits of the number *numstr* """
    summation = 0
    for digit in list(numstr):
        summation += int(digit)
    return summation


def analyse_divsums(number_str, count):
    number = int(number_str)
    sum_digit = digit_summation(number_str)
    format_dig = "{:10s} ! {:>20s} : {:<20d} = {:<20g}"
    ratio = number / sum_digit

    if number % sum_digit == 0:
        logger.info(format_dig.format("DIVIDE", number_str, sum_digit, ratio))
        count += 1
    else:
        logger.debug(format_dig.format("NO DIVIDE", number_str, sum_digit, ratio))

    return count


def analyse_palindromes(number_str, count):
    """ Check if the number *number_str* is a palindrome """
    number = int(number_str)
    number_invert = number_str[-1::-1]
    number_sum = number + int(number_invert)
    format_str = "{:20s} : {:>20s} + {:<20s} = {:<20s}"
    if is_palindrome(number_sum):
        logger.info(format_str.format("PALINDROME", number_str, number_invert, str(number_sum)))
        count += 1
    else:
        logger.debug(
            format_str.format("NO PALINGDROME", number_str, number_invert, str(number_sum)))

    return count


def main(args_in):
    args, parser = parse_the_argument(args_in)

    start = args.start
    n_number = args.number
    end = start + n_number
    dividables = False
    palindromes = False
    if args.mode == "palindrome":
        palindromes = True
    elif args.mode == "divide":
        dividables = True
    else:
        raise AssertionError(f"mode must be palindrome or divide. Found {args.mode}")

    global logger
    logging.basicConfig(level=args.log_level, format="%(levelname)8s:  %(message)s")
    logger = logging.getLogger(__name__)

    count_palindromes = 0
    count_divsums = 0
    checked = 0

    for number in range(start, end):
        checked += 1
        number_string = f"{number}"

        if dividables:
            count_divsums = analyse_divsums(number_string, count_divsums)

        if palindromes:
            count_palindromes = analyse_palindromes(number_string, count_palindromes)

    if palindromes:
        n_palindromes = calculate_n_palindromes(start)
        logger.info(f"Found {count_palindromes}/{checked} palindromes ({n_palindromes})")
        if logger.getEffectiveLevel() > logging.INFO:
            print(f"{start} {end} {count_palindromes} {n_palindromes}")

    if dividables:
        logger.info(f"Found {count_divsums}/{checked} dividables")


def _run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    _run()
